package com.example.rares.expenses.db;

import com.example.rares.expenses.vo.ExpenseAction;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface ExpenseActionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveOne(ExpenseAction expenseAction);

    @Query("DELETE FROM expenseaction")
    void deleteAll();

    @Query("DELETE FROM expenseaction WHERE id = :expenseActionId")
    void deleteByExpenseActionId(int expenseActionId);

    @Query("SELECT * FROM expenseaction ORDER BY timestamp ASC")
    List<ExpenseAction> load();
}

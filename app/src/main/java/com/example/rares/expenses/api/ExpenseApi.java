package com.example.rares.expenses.api;

import com.example.rares.expenses.vo.Expense;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import static com.example.rares.expenses.api.ApiUrls.BASE_HTTP;

public interface ExpenseApi {
    String BASE_URL = BASE_HTTP + "/";

    @GET("expense")
    Call<List<Expense>> getExpenses();

    @DELETE("expense/{id}")
    Call<Void> deleteExpense(@Path("id") String id);

    @POST("expense")
    Call<Void> addExpense(@Body Expense expense);
}

package com.example.rares.expenses.api;

import com.example.rares.expenses.vo.Expense;
import com.example.rares.expenses.vo.Token;
import com.example.rares.expenses.vo.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import static com.example.rares.expenses.api.ApiUrls.BASE_HTTP;

public interface AuthApi {
    String BASE_URL = BASE_HTTP + "/auth/";

    @POST("login")
    Call<Token> login(@Body User user);
}

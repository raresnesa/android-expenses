package com.example.rares.expenses.ui;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rares.expenses.LoginActivity;
import com.example.rares.expenses.MainActivity;
import com.example.rares.expenses.R;
import com.example.rares.expenses.repository.AuthRepository;
import com.example.rares.expenses.viewmodel.ExpensesViewModel;
import com.example.rares.expenses.vo.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class ExpenseListFragment extends Fragment {
    private static final String TAG = ExpenseListFragment.class.getCanonicalName();

    private ExpensesViewModel mViewModel;
    private RecyclerView mExpenseList;

    public static ExpenseListFragment newInstance() {
        return new ExpenseListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.expense_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mExpenseList = view.findViewById(R.id.expense_list);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mExpenseList.setLayoutManager(new LinearLayoutManager(getActivity()));

        mViewModel = ViewModelProviders.of(this, new ExpensesViewModel.Factory(getContext())).get(ExpensesViewModel.class);
        mViewModel.getExpenses().observe(this, expenses -> {
            ExpenseListAdapter expensesAdapter = new ExpenseListAdapter(getActivity(), expenses);
            mExpenseList.setAdapter(expensesAdapter);
        });

        SwipeController swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                ExpenseListAdapter adapter = (ExpenseListAdapter) mExpenseList.getAdapter();
                String expenseId = adapter.expenses.get(position).get_id();
                adapter.expenses.remove(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, adapter.getItemCount());
                mViewModel.removeExpense(expenseId);
            }
        });
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(mExpenseList);

        mExpenseList.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }

}

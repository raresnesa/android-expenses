package com.example.rares.expenses.ui;

public abstract class SwipeControllerActions {
    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}
}

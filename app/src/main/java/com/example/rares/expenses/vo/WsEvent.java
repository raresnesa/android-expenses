package com.example.rares.expenses.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WsEvent {
    @SerializedName("event")
    @Expose
    private String event;

    @SerializedName("payload")
    @Expose
    private Expense payload;

    public WsEvent(String event, Expense payload) {
        this.event = event;
        this.payload = payload;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Expense getPayload() {
        return payload;
    }

    public void setPayload(Expense payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "WsEvent{" +
                "event='" + event + '\'' +
                ", payload=" + payload +
                '}';
    }
}

package com.example.rares.expenses.vo;

import androidx.room.TypeConverter;

public class ActionTypeConverter {
    @TypeConverter
    public static String toString(ExpenseAction.ActionType actionType) {
        return actionType.toString();
    }

    @TypeConverter
    public static ExpenseAction.ActionType toActionType(String str) {
        return ExpenseAction.ActionType.valueOf(str);
    }
}
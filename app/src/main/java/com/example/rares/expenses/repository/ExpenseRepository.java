package com.example.rares.expenses.repository;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import com.example.rares.expenses.api.ExpenseApi;
import com.example.rares.expenses.db.ExpenseActionDao;
import com.example.rares.expenses.db.ExpenseDao;
import com.example.rares.expenses.db.ExpenseDatabase;
import com.example.rares.expenses.db.TokenDatabase;
import com.example.rares.expenses.utils.Provider;
import com.example.rares.expenses.utils.UiUtils;
import com.example.rares.expenses.viewmodel.ExpensesViewModel;
import com.example.rares.expenses.vo.Expense;
import com.example.rares.expenses.vo.ExpenseAction;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executor;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.rares.expenses.api.ApiUrls.BASE_WS;

public class ExpenseRepository {
    private static final String TAG = ExpenseRepository.class.getCanonicalName();
    private static ExpenseRepository instance;

    public static ExpenseRepository getInstance(Context context) {
        if (instance == null) {
            instance = new ExpenseRepository(context);
        }
        return instance;
    }

    private TokenDatabase tokenDatabase;
    private ExpenseApi expenseApi;
    private ExpenseDao expenseDao;
    private ExpenseActionDao expenseActionDao;
    private Executor executor;
    private Context context;
    private ConnectivityNetworkCallback callback;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private boolean connected = false; // connection to internet

    private OkHttpClient client;
    private WebSocket ws;

    @SuppressLint("CheckResult")
    public ExpenseRepository(Context context) {
        this.context = context;
        this.tokenDatabase = TokenDatabase.getInstance(context);
        ExpenseDatabase expenseDatabase = ExpenseDatabase.getInstance(context);
        this.expenseDao = expenseDatabase.expenseDao();
        this.expenseActionDao = expenseDatabase.expenseActionDao();
        this.executor = Provider.getExecutor();
        this.client = new OkHttpClient();
        this.callback = ConnectivityNetworkCallback.getInstance();

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request.Builder ongoing = chain.request().newBuilder();
                    ongoing.addHeader("Accept", "application/json");
                    String token = tokenDatabase.getToken();
                    if (token != null) {
                        ongoing.addHeader("Authorization", "Bearer " + token);
                    }
                    return chain.proceed(ongoing.build());
                })
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ExpenseApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        expenseApi = retrofit.create(ExpenseApi.class);

        compositeDisposable.add(callback.getConnection$().subscribe(this::onConnectionChanged));
    }

    public LiveData<List<Expense>> getExpenses() {
        // user logged
        checkAndExecuteActions();
        return expenseDao.load();
    }

    private void onConnectionChanged(boolean connected) {
        this.connected = connected;
        boolean isLogged = this.tokenDatabase.getToken() != null;
        if (!isLogged) return;

        if (connected) {
            checkAndExecuteActions();
        } else {
            disconnectWebsocket();
        }
    }

    private void checkAndExecuteActions() {
        executor.execute(() -> {
            syncExpenses();

            List<ExpenseAction> expenseActions = this.expenseActionDao.load();

            if (expenseActions == null) {
                Log.d(TAG, "========== OFFLINE LIST is null =========");
                return;
            }

            expenseActions.forEach(action -> {
                switch (action.getActionType()) {
                    case ADD:
                        addExpense(new Expense(action.getExpenseId(), action.getExpenseItem(), action.getExpensePrice()));
                        break;
                    case DELETE:
                        removeExpense(action.getExpenseId());
                        break;
                }
                this.expenseActionDao.deleteByExpenseActionId(action.getId());
            });
        });
    }

    private void connectWebsocket() {
        Request request = new Request.Builder().url(BASE_WS).build();
        ExpenseWebSocketListener listener = new ExpenseWebSocketListener(this.context);
        ws = client.newWebSocket(request, listener);
    }

    void disconnectWebsocket() {
        if (ws != null) {
            ws.close(1000, "User logged out");
        }
    }

    private void syncExpenses() {
        Call<List<Expense>> call = expenseApi.getExpenses();
        call.enqueue(new Callback<List<Expense>>() {
            @Override
            public void onResponse(@NonNull Call<List<Expense>> call, @NonNull Response<List<Expense>> response) {
                if (response.code() != 200) {
                    Log.d(TAG, "Load expenses failed: " + response.message());
                    return;
                }
                Log.d(TAG, "Load expenses succeeded");
                response.body().sort(Comparator.comparingDouble(Expense::getPrice).reversed());
                executor.execute(() -> {
                    expenseDao.deleteAll();
                    expenseDao.save(response.body());
                    UiUtils.showToast(context, "Synced expenses from server");
                    connectWebsocket();
                });
            }

            @Override
            public void onFailure(@NonNull Call<List<Expense>> call, @NonNull Throwable t) {
                Log.e(TAG, "Load expenses failed", t);
                UiUtils.showToast(context, "Expenses sync failed.");
            }
        });
    }

    public void removeExpense(String expenseId) {
        executor.execute(() -> {
            if (connected) {
                Call<Void> call = expenseApi.deleteExpense(expenseId);
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                        if (response.code() != 204) {
                            Log.d(TAG, "Delete expense failed: " + response.message());
                            UiUtils.showToast(context, "Expense deletion failed.");
                            return;
                        }
                        Log.d(TAG, "Delete expense succeeded");
                    }

                    @Override
                    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                        Log.e(TAG, "Delete expense failed", t);
                        UiUtils.showToast(context, "Expense deletion failed.");
                    }
                });
            } else {
                Log.d(TAG, "SAVING OFFLINE DELETE");
                long timestamp = Instant.now().getEpochSecond();
                this.expenseActionDao.saveOne(
                        new ExpenseAction(ExpenseAction.ActionType.DELETE,
                                timestamp,
                                expenseId)
                );
                this.expenseDao.deleteByExpenseId(expenseId);
                UiUtils.showToast(context, "An expense was deleted.");
            }
        });
    }

    public void addExpense(Expense expense) {
        executor.execute(() -> {
            if (connected) {
                Call<Void> call = expenseApi.addExpense(expense);
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                        if (response.code() != 200) {
                            Log.d(TAG, "Add expense failed: " + response.message());
                            UiUtils.showToast(context, "Expense add failed.");
                            return;
                        }
                        Log.d(TAG, "Expense add succeeded");
                    }

                    @Override
                    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                        Log.e(TAG, "Add expense failed", t);
                        UiUtils.showToast(context, "Expense add failed.");
                    }
                });
            } else {
                long timestamp = Instant.now().getEpochSecond();
                this.expenseActionDao.saveOne(
                        new ExpenseAction(ExpenseAction.ActionType.ADD,
                                timestamp,
                                expense.get_id(),
                                expense.getItem(),
                                expense.getPrice())
                );
                this.expenseDao.saveOne(expense);
                UiUtils.showToast(context, "An expense was created.");
            }
        });
    }
}

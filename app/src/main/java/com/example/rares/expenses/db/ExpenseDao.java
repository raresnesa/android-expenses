package com.example.rares.expenses.db;

import com.example.rares.expenses.vo.Expense;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface ExpenseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(List<Expense> expense);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveOne(Expense expense);

    @Update()
    void updateOne(Expense expense);

    @Query("DELETE FROM expense")
    void deleteAll();

    @Query("DELETE FROM expense WHERE _id = :expenseId")
    void deleteByExpenseId(String expenseId);

    @Query("SELECT * FROM expense ORDER BY price DESC")
    LiveData<List<Expense>> load();

}

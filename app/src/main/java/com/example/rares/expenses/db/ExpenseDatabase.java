package com.example.rares.expenses.db;

import android.app.Application;
import android.content.Context;

import com.example.rares.expenses.vo.ActionTypeConverter;
import com.example.rares.expenses.vo.Expense;
import com.example.rares.expenses.vo.ExpenseAction;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Expense.class, ExpenseAction.class}, version = 1)
@TypeConverters({ActionTypeConverter.class})
public abstract class ExpenseDatabase extends RoomDatabase {

    private static ExpenseDatabase instance;

    public static ExpenseDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context,
                    ExpenseDatabase.class, "ExpenseDatabase.db")
                    .build();
        }
        return instance;
    }

    public abstract ExpenseDao expenseDao();

    public abstract ExpenseActionDao expenseActionDao();
}

package com.example.rares.expenses.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.time.Instant;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Expense {

    @PrimaryKey
    @NonNull
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("item")
    @Expose
    private String item;

    @SerializedName("price")
    @Expose
    private Double price;

    public Expense() {
    }

    @Ignore
    public Expense(@NonNull String _id, String item, Double price) {
        this._id = _id;
        this.item = item;
        this.price = price;
    }

    @Ignore
    public Expense(String item, Double price) {
        this._id = String.valueOf(Instant.now().getEpochSecond());
        this.item = item;
        this.price = price;
    }

    @Ignore
    public Expense(@NonNull String _id) {
        this._id = _id;
    }

    @NonNull
    public String get_id() {
        return _id;
    }

    public void set_id(@NonNull String _id) {
        this._id = _id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        if (item == null && price == null)
            return _id;
        return _id + ";" + item + ";" + price;
    }

    static Expense fromString(String str) {
        String[] args = str.split(";");
        if (args.length == 1) return new Expense(args[0]);
        return new Expense(args[0], args[1], Double.parseDouble(args[2]));
    }
}

package com.example.rares.expenses;

import androidx.appcompat.app.AppCompatActivity;
import io.reactivex.disposables.CompositeDisposable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.rares.expenses.repository.AuthRepository;
import com.example.rares.expenses.repository.ConnectivityNetworkCallback;
import com.example.rares.expenses.ui.ExpenseListFragment;
import com.example.rares.expenses.utils.UiUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getCanonicalName();
    private AuthRepository authRepository;
    private ConnectivityManager manager;
    private ConnectivityNetworkCallback callback;
    private View activityMainView;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);
        activityMainView = findViewById(R.id.main);

        setSupportActionBar(findViewById(R.id.my_toolbar));
        this.authRepository = AuthRepository.getInstance(getApplicationContext());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> changeActivityToAdd());

        registerOnlineOffline();

        compositeDisposable.add(callback.getConnection$().subscribe(connected -> {
            if (connected)
                UiUtils.showSnackbar(activityMainView, "You are online.", false);
            else
                UiUtils.showSnackbar(activityMainView, "You are offline.", true);
        }));

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, ExpenseListFragment.newInstance())
                    .commitNow();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.overflow_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                // User chose the "Logout" item
                this.onLogout();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private void registerOnlineOffline() {
        manager = (ConnectivityManager) this
                .getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        callback = ConnectivityNetworkCallback.getInstance();
        manager.registerDefaultNetworkCallback(callback);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void onLogout() {
        Log.d(TAG, "On logout");
        authRepository.logout(this::changeActivity);
    }

    private void changeActivity() {
        Intent myIntent = new Intent(this, LoginActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
    }

    private void changeActivityToAdd() {
        Intent myIntent = new Intent(this, AddActivity.class);
        startActivity(myIntent);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }
}

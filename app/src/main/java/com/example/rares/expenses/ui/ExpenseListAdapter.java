package com.example.rares.expenses.ui;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rares.expenses.R;
import com.example.rares.expenses.vo.Expense;

import java.util.List;

public class ExpenseListAdapter extends RecyclerView.Adapter<ExpenseListAdapter.ExpenseViewHolder> {

    private Context context;
    List<Expense> expenses;

    public ExpenseListAdapter(Context context, List<Expense> expenses) {
        this.context = context;
        this.expenses = expenses;
    }

    @NonNull
    @Override
    public ExpenseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.expense_layout, parent, false);
        return new ExpenseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpenseViewHolder expenseViewHolder, int i) {
        Expense expense = expenses.get(i);
        expenseViewHolder.itemName.setText(expense.getItem());
        expenseViewHolder.price.setText(String.valueOf(expense.getPrice()));
    }

    @Override
    public int getItemCount() {
        if (expenses != null)
            return expenses.size();
        return 0;
    }

    class ExpenseViewHolder extends RecyclerView.ViewHolder {
        TextView itemName;
        TextView price;

        ExpenseViewHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.text_item);
            price = itemView.findViewById(R.id.text_price);
        }
    }


}

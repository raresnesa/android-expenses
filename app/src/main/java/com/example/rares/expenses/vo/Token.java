package com.example.rares.expenses.vo;

import androidx.annotation.NonNull;

public class Token {
    @NonNull
    String token;

    public Token(@NonNull String token) {
        this.token = token;
    }

    public Token() {
    }

    @NonNull
    public String getToken() {
        return token;
    }

    public void setToken(@NonNull String token) {
        this.token = token;
    }
}

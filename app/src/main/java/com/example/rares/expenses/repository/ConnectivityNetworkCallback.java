package com.example.rares.expenses.repository;

import android.net.ConnectivityManager;
import android.net.Network;
import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ConnectivityNetworkCallback extends ConnectivityManager.NetworkCallback {
    private static final String TAG = ConnectivityNetworkCallback.class.getCanonicalName();
    private final PublishSubject<Boolean> connectionSubject = PublishSubject.create();

    private static ConnectivityNetworkCallback instance;
    public static ConnectivityNetworkCallback getInstance() {
        if (instance == null) {
            instance = new ConnectivityNetworkCallback();
        }
        return instance;
    }

    private ConnectivityNetworkCallback() {
    }

    public Observable<Boolean> getConnection$() {
        return connectionSubject;
    }

    @Override
    public void onAvailable(Network network) {
        super.onAvailable(network);
        Log.d(TAG, "Online");
        connectionSubject.onNext(true);
    }

    @Override
    public void onLost(Network network) {
        super.onLost(network);
        Log.d(TAG, "Offline");
        connectionSubject.onNext(false);
    }
}

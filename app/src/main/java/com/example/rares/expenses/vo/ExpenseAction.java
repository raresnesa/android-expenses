package com.example.rares.expenses.vo;

import android.drm.DrmStore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.time.Instant;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

@Entity
public class ExpenseAction {

    public enum ActionType {
        ADD, DELETE
    }

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("action")
    @Expose
    private ActionType actionType;

    @SerializedName("timestamp")
    @Expose
    private long timestamp;

    @SerializedName("expenseId")
    @Expose
    private String expenseId;

    @SerializedName("expenseItem")
    @Expose
    private String expenseItem;

    @SerializedName("expensePrice")
    @Expose
    private Double expensePrice;

    public ExpenseAction() {
    }

    @Ignore
    public ExpenseAction(@NonNull int id, ActionType actionType, long timestamp, String expenseId, String expenseItem, Double expensePrice) {
        this.id = id;
        this.actionType = actionType;
        this.timestamp = timestamp;
        this.expenseId = expenseId;
        this.expenseItem = expenseItem;
        this.expensePrice = expensePrice;
    }

    @Ignore
    public ExpenseAction(ActionType actionType, long timestamp, String expenseId, String expenseItem, Double expensePrice) {
        this.actionType = actionType;
        this.timestamp = timestamp;
        this.expenseId = expenseId;
        this.expenseItem = expenseItem;
        this.expensePrice = expensePrice;
    }

    @Ignore
    public ExpenseAction(ActionType actionType, long timestamp, String expenseId) {
        this.actionType = actionType;
        this.timestamp = timestamp;
        this.expenseId = expenseId;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseItem() {
        return expenseItem;
    }

    public void setExpenseItem(String expenseItem) {
        this.expenseItem = expenseItem;
    }

    public Double getExpensePrice() {
        return expensePrice;
    }

    public void setExpensePrice(Double expensePrice) {
        this.expensePrice = expensePrice;
    }

    @Override
    public String toString() {
        return "ExpenseAction{" +
                "id=" + id +
                ", actionType=" + actionType +
                ", timestamp=" + timestamp +
                ", expenseId='" + expenseId + '\'' +
                ", expenseItem='" + expenseItem + '\'' +
                ", expensePrice='" + expensePrice + '\'' +
                '}';
    }
}

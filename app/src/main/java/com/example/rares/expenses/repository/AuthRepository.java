package com.example.rares.expenses.repository;

import android.content.Context;
import android.util.Log;

import com.example.rares.expenses.api.AuthApi;
import com.example.rares.expenses.db.TokenDatabase;
import com.example.rares.expenses.vo.Token;
import com.example.rares.expenses.vo.User;

import java.util.function.Consumer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AuthRepository {
    private static final String TAG = AuthRepository.class.getCanonicalName();
    private final ExpenseRepository expenseRepository;
    private TokenDatabase tokenDatabase;
    private Retrofit retrofit;
    private AuthApi authApi;

    private static AuthRepository instance;
    public static AuthRepository getInstance(Context context) {
        if (instance == null) {
            instance = new AuthRepository(context);
        }
        return instance;
    }

    private AuthRepository(Context context) {
        this.tokenDatabase = TokenDatabase.getInstance(context);
        this.expenseRepository = ExpenseRepository.getInstance(context);

        retrofit = new Retrofit.Builder()
                .baseUrl(AuthApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        authApi = retrofit.create(AuthApi.class);
    }

    public boolean isLogged() {
        return tokenDatabase.getToken() != null;
    }

    public void login(User user, Consumer<String> onSuccess, Consumer<String> onFail) {
        Call<Token> call = authApi.login(user);
        Log.d(TAG, "Login call");

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.body() == null) {
                    Log.d(TAG, "Login failed");
                    onFail.accept("Invalid credentials");
                    return;
                }

                String token = response.body().getToken();
                Log.d(TAG, "Login succeeded : " + token);
                tokenDatabase.insertToken(token); // save token in shared preferences
                onSuccess.accept(token);
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                Log.d(TAG, "Login failed");
                onFail.accept(t.getMessage());
            }
        });
    }

    public void logout(Runnable callback) {
        Log.d(TAG, "Logout call");
        this.expenseRepository.disconnectWebsocket();
        tokenDatabase.deleteToken();
        callback.run();
    }
}

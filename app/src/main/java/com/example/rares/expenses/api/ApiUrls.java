package com.example.rares.expenses.api;

public class ApiUrls {
//    public static final String BASE_IP = "192.168.56.1:3000";
    public static final String BASE_IP = "192.168.43.49:3000";
    public static final String BASE_HTTP = "http://" + BASE_IP + "/api";
    public static final String BASE_WS = "ws://" + BASE_IP + "/api";
}

package com.example.rares.expenses;

import androidx.lifecycle.ViewModelProviders;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.rares.expenses.viewmodel.LoginViewModel;
import com.example.rares.expenses.vo.User;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getCanonicalName();

    private LoginViewModel loginViewModel;
    private EditText usernameText;
    private EditText passwordText;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameText = findViewById(R.id.username);
        passwordText = findViewById(R.id.password);
        loginButton = findViewById(R.id.login);
        loginViewModel = ViewModelProviders.of(this, new LoginViewModel.Factory(getApplicationContext())).get(LoginViewModel.class);

        if (loginViewModel.isLogged()) {
            Log.d(TAG, "USER IS LOGGED");
            changeActivity();
        }

        loginButton.setOnClickListener(this::onLogin);
    }

    private void onLogin(View view) {
        final String username = usernameText.getText().toString();
        final String password = passwordText.getText().toString();
        final User user = new User(username, password);
        Log.d(TAG, user.toString());

        loginViewModel.login(
                user,
                (token) -> changeActivity(),
                (error) -> {}
        );
    }

    private void changeActivity() {
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }

}

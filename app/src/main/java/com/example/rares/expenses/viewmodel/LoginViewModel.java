package com.example.rares.expenses.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import android.content.Context;
import android.util.Log;

import com.example.rares.expenses.api.AuthApi;
import com.example.rares.expenses.db.TokenDatabase;
import com.example.rares.expenses.repository.AuthRepository;
import com.example.rares.expenses.vo.Token;
import com.example.rares.expenses.vo.User;

import java.util.function.Consumer;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginViewModel extends ViewModel {

    private AuthRepository authRepository;

    public LoginViewModel(Context ctxt) {
        this.authRepository = AuthRepository.getInstance(ctxt);
    }

    public boolean isLogged() {
        return authRepository.isLogged();
    }

    public void login(User user, Consumer<String> onSuccess, Consumer<String> onFail) {
        this.authRepository.login(user, onSuccess, onFail);
    }

    public static class Factory implements ViewModelProvider.Factory {
        private final Context ctxt;

        public Factory(Context ctxt) {
            this.ctxt=ctxt.getApplicationContext();
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return((T)new LoginViewModel(ctxt));
        }
    }
}

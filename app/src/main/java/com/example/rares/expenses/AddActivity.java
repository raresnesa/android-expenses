package com.example.rares.expenses;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.rares.expenses.utils.UiUtils;
import com.example.rares.expenses.viewmodel.ExpensesViewModel;
import com.example.rares.expenses.vo.Expense;

public class AddActivity extends AppCompatActivity {
    private static final String TAG = AppCompatActivity.class.getCanonicalName();

    private Toolbar mToolbar;
    private ExpensesViewModel mViewModel;

    private EditText mItemName;
    private EditText mItemPrice;
    private Button mButtonAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        mToolbar = findViewById(R.id.toolbar_add);
        mItemName = findViewById(R.id.item_name);
        mItemPrice = findViewById(R.id.item_price);
        mButtonAdd = findViewById(R.id.button_add);

        mButtonAdd.setOnClickListener(v -> onAdd());

        mToolbar.setTitle("Add expense");
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mViewModel = ViewModelProviders
                .of(this, new ExpensesViewModel.Factory(getApplicationContext()))
                .get(ExpensesViewModel.class);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void onAdd() {
        String itemName = mItemName.getText().toString();
        String itemPrice = mItemPrice.getText().toString();
        try {
            Expense expense = new Expense(itemName, Double.parseDouble(itemPrice));
            mViewModel.addExpense(expense);
            finish();
        } catch (NumberFormatException ex) {
            UiUtils.showToast(getApplicationContext(), "Invalid price");
        }
    }
}

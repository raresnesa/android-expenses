package com.example.rares.expenses.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import com.example.rares.expenses.R;
import com.google.android.material.snackbar.Snackbar;

import androidx.core.content.ContextCompat;

public class UiUtils {
    public static void showToast(final Context context, final String text) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> Toast.makeText(context, text, Toast.LENGTH_LONG).show());
    }

    public static void showSnackbar(final View view, final String text, boolean warning) {
        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_SHORT)
                .setAction("", null);
        View snackbarView = snackbar.getView();
        if (warning)
            snackbarView.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorDanger));
        else
            snackbarView.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorAccent));
        snackbar.show();
    }
}

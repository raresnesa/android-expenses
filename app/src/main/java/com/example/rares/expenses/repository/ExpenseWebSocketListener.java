package com.example.rares.expenses.repository;

import android.content.Context;
import android.util.Log;

import com.example.rares.expenses.db.ExpenseDao;
import com.example.rares.expenses.db.ExpenseDatabase;
import com.example.rares.expenses.db.TokenDatabase;
import com.example.rares.expenses.utils.Provider;
import com.example.rares.expenses.utils.UiUtils;
import com.example.rares.expenses.viewmodel.ExpensesViewModel;
import com.example.rares.expenses.vo.WsEvent;
import com.google.gson.Gson;

import java.util.concurrent.Executor;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class ExpenseWebSocketListener extends WebSocketListener {
    private static final int NORMAL_CLOSURE_STATUS = 1000;
    private static final String TAG = ExpenseWebSocketListener.class.getCanonicalName();
    private final ExpenseDao expenseDao;
    private final Executor executor;
    private final Gson gson;
    private final Context context;
    private TokenDatabase tokenDatabase;

    public ExpenseWebSocketListener(Context context) {
        this.context = context;
        this.tokenDatabase = TokenDatabase.getInstance(context);
        ExpenseDatabase expenseDatabase = ExpenseDatabase.getInstance(context);
        this.expenseDao = expenseDatabase.expenseDao();
        this.executor = Provider.getExecutor();
        this.gson = Provider.getGson();
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send(tokenDatabase.getToken());
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        executor.execute(() -> {
            WsEvent event = gson.fromJson(text, WsEvent.class);
            Log.d(TAG, "Receiving: " + event.toString());

            switch (event.getEvent()) {
                case "expense/created":
                    this.expenseDao.saveOne(event.getPayload());
                    UiUtils.showToast(context, "An expense was created.");
                    break;
                case "expense/updated":
                    this.expenseDao.updateOne(event.getPayload());
                    UiUtils.showToast(context, "An expense was updated.");
                    break;
                case "expense/deleted":
                    this.expenseDao.deleteByExpenseId(event.getPayload().get_id());
                    UiUtils.showToast(context, "An expense was deleted.");
                    break;
            }
        });
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        Log.d(TAG, "Receiving bytes : " + bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        Log.d(TAG, "Closing : " + code + " / " + reason);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        Log.d(TAG, "Error : " + t.getMessage());
    }
}

package com.example.rares.expenses.db;

import android.content.Context;
import android.preference.PreferenceManager;

public class TokenDatabase {

    private static TokenDatabase instance;
    private String token = null;
    private Context ctxt;

    private TokenDatabase(Context ctxt) {
        this.ctxt = ctxt;
    }

    public static TokenDatabase getInstance(Context ctxt) {
        if (instance == null) {
            instance = new TokenDatabase(ctxt);
        }
        return instance;
    }

    public String getToken() {
        if (token != null) return token;
        return PreferenceManager
                .getDefaultSharedPreferences(ctxt)
                .getString("token", null);
    }

    public void insertToken(String token) {
        PreferenceManager
                .getDefaultSharedPreferences(ctxt)
                .edit()
                .putString("token", token)
                .apply();
        this.token = token;
    }

    public void deleteToken() {
        this.token = null;
        PreferenceManager
                .getDefaultSharedPreferences(ctxt)
                .edit()
                .remove("token")
                .apply();
    }
}

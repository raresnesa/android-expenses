package com.example.rares.expenses.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;

import android.content.Context;

import com.example.rares.expenses.repository.ExpenseRepository;
import com.example.rares.expenses.vo.Expense;

import java.util.List;

import androidx.lifecycle.ViewModelProvider;

public class ExpensesViewModel extends ViewModel {
    private static final String TAG = ExpensesViewModel.class.getCanonicalName();

    private ExpenseRepository expenseRepository;

    public ExpensesViewModel(Context ctxt) {
        this.expenseRepository = ExpenseRepository.getInstance(ctxt);
    }

    public LiveData<List<Expense>> getExpenses() {
        return expenseRepository.getExpenses();
    }

    public void removeExpense(String expenseId) {
        expenseRepository.removeExpense(expenseId);
    }

    public void addExpense(Expense expense) {
        expenseRepository.addExpense(expense);
    }


    public static class Factory implements ViewModelProvider.Factory {
        private final Context ctxt;

        public Factory(Context ctxt) {
            this.ctxt = ctxt.getApplicationContext();
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return ((T) new ExpensesViewModel(ctxt));
        }
    }
}
